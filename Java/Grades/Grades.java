/* 
 * Name: Connor Neuhaus
 * e-name: neuhaus
 * date: 10/19/2009
 * Course: CS160
 */

import java.util.*;
public class Grades { 
     public static void main(String[] args) {
    	  Scanner sc = new Scanner(System.in);
    	  System.out.println("Welcome to the Grade Calculator Program");
    	  System.out.println("Please enter the student scores as integers between 0 and 100.");
    	  System.out.println("Separate the scores with a comma.");
    	  System.out.println("Follow the last student score with a negative number.");
    	  System.out.println("Enter the scores here: ");
    	  
    	  String scores = sc.nextLine();
    	  String delims = "[\\p{Punct}]+";
          String[] array = scores.split(delims); 
          System.out.println("The scores are: {" + scores+"}");
          System.out.println("");
          System.out.println("Grade Calculations: ");
          int[] array1 = new int[array.length];
          int max=0;
          int min=100;
          int runtot=0;
          int element;
          for(element = 0;element<array.length;element++){
          array1[element]=Integer.parseInt(array[element]);
          runtot=runtot+array1[element];
           if(array1[element]>max)
           {
        	  max=array1[element];
           }
           if(array1[element]<min){
        	  min=array1[element];
           }
           if((array1[element]<101) && (array1[element]>89)){
        	   
        	//   System.out.println("Number of A's = "+A);
           }
          }
          int total = element;
          double fun= runtot/total;
          System.out.println("Total number of grades = "+total);
          System.out.println("Low score = "+min);
          System.out.println("High score = "+max);
          System.out.println("Average score = "+fun);
          }
     }
    	  
          /*
    	  gradesIN =
    	  
    	  double average = ((Math.sum(gradesIN))/total)
    	  
    	  System.out.println("");
    	  System.out.println("Grade Calculations:");
    	  
    	  System.out.println("Low score = "+low);
    	  System.out.println("High score = "+high);
    	  System.out.println("Average score = "+average);
    	  System.out.println("Number of A's = "+numA);
    	  System.out.println("Number of B's = "+numB);
    	  System.out.println("Number of C's = "+numC);
    	  System.out.println("Number of D's = "+numD);
    	  System.out.println("Number of F's = "+numF);
    	  
    	  
     }
}*/
