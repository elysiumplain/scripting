import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.NoSuchElementException;
import java.util.Calendar;

/** This is the base class for Assignment 1. Do <b>NOT</b> 
 * change anything in this file. When grading, a different version of this
 * file <b>MAY</b> be used, so any changes you make would be lost.
 * <p>
 * This file contains the declarations and documentation for the methods
 * you will write in the class <code>MyAssignment1.java</code>. Study
 * the documentation carefully.
 * <p>
 * In  writing your code, you may be tempted to use <code>System.out.println()
 * </code> to determine what is happening. As an alterntive, the method
 * <code>trace()</code> is provided. The nice thing about <code>trace()</code>
 * in comparison to <code>System.out.println()</code> is that <code>trace()
 * </code> <b>only</b> prints when debug is turn <b>on</b>. You can turn
 * debug on and off by using the <code>debug</code> command in the program.
 * Thus, you can use it, but not need to change your program in any way before
 * turning it in. If you use <code>System.out.println()</code> you <b>MUST</b>
 * either comment out the lines or remove them before you submit your code
 * for grading.
 * <p>
 * <h2>Getting Started</h2>
 * When you run this class, it wil print <code>Enter commands:</code>. Type
 * <code>help</code> and you will see a list of the commands available. Type in
 * the name of a command followed by any parameters required. For example
 * to test if the value 15 is odd, type <code>odd 15</code>. The program
 * will respond <code>isOdd(15) is false</code>. When you have correctly
 * implemented the method <code>MyAssignment1.isOdd()</code>, you will get
 * the correct results. There is a command to allow you to test <b>each</b>
 * method you must implement.
 */

public abstract class Assign1 {
	
  private static int[] daysInMonths;
  private static int[] cumulative;

  /** Any non-zero value turns debugging <b>on</b> */
  public int debug = 0;

  /** Print a message if the debug value is non-zero
   * @param msg - the message to be printed
   */
  public void trace (String msg) {
    if (debug != 0)
      System.out.println(msg);
  }

  /** Print an  message and one dimensional array on a single line
   * @param msg a message to preceeed to output
   * @param A one dimensional array to be printed
   * post: A printed on one line
   */
  public void print1D(String msg, int[] A) {	
    System.out.println(msg + ": " + Arrays.toString(A));
  }
	
  /** prints all triples
   * <cite>(i,j,k)</cite> for which <cite>0 <= i <= n-1</cite>,
   * <cite>0 <= j <= n-1</cite>,  and
   * <cite>0 <= k <= n-1</cite>.
   * <p>
   * For example, if <cite>n=3</cite>, your code should print
   * <pre>
   * [0, 0, 0] [0, 0, 1] [0, 0, 2]
   * [0, 1, 0] [0, 1, 1] [0, 1, 2]
   * [0, 2, 0] [0, 2, 1] [0, 2, 2]
   * [1, 0, 0] [1, 0, 1] [1, 0, 2]
   * [1, 1, 0] [1, 1, 1] [1, 1, 2]
   * [1, 2, 0] [1, 2, 1] [1, 2, 2]
   * [2, 0, 0] [2, 0, 1] (2, 0, 2]
   * [2, 1, 0] [2, 1, 1] [2, 1, 2]
   * [2, 2, 0] [2, 2, 1] [2, 2, 2]
   *
   * pre: n>=0
   * post: all ordered triples [i, j, k] are printed, such that
   *       i in 0, 1, ..., n-1, 
   *       j in 0, 1, ..., n-1,
   *       k in 0, 1, ..., n-1
   * </pre>
   * <br>
   * Put the values of one answer in an int[] and use <code>Arrays.toString()
   * </code> to print the array.
   * @param n set size
   */
   public abstract void printOrderedTriples (int n);
	
   /** Print subset of the triples
    * In the method printOrderedTriples, the order of the elements matters.
    * In other words, <code>(0,1,2)</code> is not the same triple
    * as <code>(2,0,1)</code>.  However, for sets of, say, three integer
    * elements, the order in which the elements are listed is not relevant.
    * Therefore, <cite>{0,1,2}</cite> is the same set
    * as <cite>{2,0,1}</cite>. Also, sets have no repeated elements, so the
    * set <cite>{0,0,0}</cite> is the same set as <cite>{0}</cite>.
    * <p>
    * This method prints all three-element subsets <cite>{i,j,k}</cite>
    * for which
    * <pre>
    * <cite>0 <= i <= n-1</cite>, 
    * <cite>0 <= j <= n-1</cite>,  and
    * <cite>0 <= k <= n-1</cite>.
    * 
    * For example, if <cite>n=5</cite>, your code should print
    * 
    * [0, 1, 2] [0, 1, 3] [0, 1, 4]
    * [0, 2, 3] [0, 2, 4]
    * [0, 3, 4]
    *
    * [1, 2, 3] [1, 2, 4]
    * [1, 3, 4]
    *
    * [2, 3, 4]
    *
    * pre: n >= 0
    * post: all subsets {i,j,k} such that
    *        i in 0, 1, ..., n-1, 
    *        j in 0, 1, ..., n-1,
    *        k in 0, 1, ..., n-1
    *       Notice {i,j,k} = {j,i,k} etc. only one of these is printed.
    * </pre>
    * <br>
    * Put the values of one answer in an int[] and use <code>Arrays.toString()
    * </code> to print the array.
    *
    * @param n set size
    */
   public abstract void printSubsets(int n);
	
   /**Determine if a value is odd
    * @param n the value to be tested
    * @return true if n is odd, false otherwise
    */
   public abstract boolean isOdd(int n);

   /** Split an input array into two piles depending on whether a given
    * element is even or odd. If there are an odd number of values in the
    * <code>source</code> array, there will be <b>one</b> more odd value
    * than even value.
    * <pre>
    * pre: source.length = evens.length + odds.length;
    *      odds.length - evens.lenght <= 1
    *      odds is empty
    *      evens is empty
    * post: all the even values are in the evens array
    *       alls the odds are in the odds array
    * </pre>
    * @param source input array
    * @param evens result array with even elements of source
    * @param odds result array with odd elements of source
    */
    public abstract void split(int[] source, int[] evens, int[] odds);

    /** Merge the two array into a single array. Alternatly take
     * values from the <code>pile1</code> and <code>pile2</code> arrays and
     * put them in the<code> dest</code> array. If there are any left over in
     * one of the arrays, add them at the end of the <code>dest</code> array.
     * <pre>
     * pre: dest.length = pile1.length + pile2.length
     * post: dest contains the union (shuffled) of the elements in the arrays
     * </pre>
     * 
     * @param dest  output array
     * @param pile1 one input array with elements for Dest
     * @param pile2 a second input array with elements for Dest
     */
    public abstract void merge(int[] dest, int[] pile1, int[] pile2);

    /** Create an array containing the number of days in each month
     * The final array contains 31 for January (at index 0),
     *  28 or 29 for February (at index 1), etc.
     * @return an initialized array
     */
    public abstract int[] getDaysInMonths();

    /** Compute the cumulative number of days from the beginning of the year
     * 2010 to the beginning of a given month. This is a helper method to make
     * the writing of the <code>date()</code> method easier. For the year 2008
     * the months array would contain 31, 29,31, etc. And the cumMonths array
     * would contain 31, 60, 91, etc. 
     * <pre>
     * pre: cumMonths.length = months.length+1
     * post: cumMonths[0] = number of days in January
     *       cumMonths[i] = cumMonths[i-1] + months[i] for i = 1, ...
     * </pre>
     * @param cumMonths output array
     * @param months input array containing number of days in each month
     */
    public abstract void makeCumulatives(int[] cumMonths, int[] months);

    /** Print the Day of week of a given day number in the year 2010.
     * <pre>
     * pre: 1 <= day M= 365
     * post: prints day of year, day of week, month and date For example:
     *
     * day 70: Mon, Mar 10
     *
     * use three letter abreviations for the week day and month
     * </pre>
     * 
     * @param day day of the year (1 to 365)
     * @param cumMonths array of cumulative Month days as computed in the call
     * <code>makeCumulatives()</code>
     */
    public abstract void date(int day, int[] cumMonths);
    
    /** Display a brief help summary */
    private void showHelp() {
      System.out.println("command summary");
      System.out.println(" cumul");
      System.out.println(" date <dayOfYear>");
      System.out.println(" days");
      System.out.println(" debug <int>");
      System.out.println(" help or ?");
      System.out.println(" input <fileName>");
      System.out.println(" merge <clsOfOdds> <cslOfEvens>");
      System.out.println(" odd <number>");
      System.out.println(" split <cslOfNumbers>");
      System.out.println(" subset <number>");
      System.out.println(" triples <number>");
    }

    /** Extract an array of ints from a comma separated list
     * @param scanner retrieve values from this scanner
     * @return array filled with values
     */
    private int[] getArray (Scanner scanner) throws NumberFormatException{
      String   csl  = scanner.next().trim();
      String[] vals = csl.split(",");
      int[] res = new int[vals.length];

      for (int i = 0; i < res.length; i++) {
        res[i] = Integer.parseInt(vals[i]);
      }

      return res;
    }

    /** Comand interpreter to test code */
    public void processCommands (Scanner lineScanner) {

       
      while (lineScanner.hasNextLine()) {
        String line = lineScanner.nextLine().trim().toLowerCase();

        if (line.startsWith("#") || (line.length() == 0))
          continue;

        Scanner cmdScanner = new Scanner(line);

        try {
          String cmd  = cmdScanner.next().trim();

          if (cmd.equals("cumul")) {
            print1D("cumulative days by month", cumulative);
          }
           
          else if (cmd.equals("date")) {
            int i = cmdScanner.nextInt();
            date(i, cumulative);
          }

          else if (cmd.equals("days")) {
            print1D("days by month", daysInMonths);
          }

          else if (cmd.equals("debug")) {
            debug = cmdScanner.nextInt();
          }

          else if (cmd.equals("exit")) {
            break;
          }

          else if (cmd.equals("help") || cmd.equals("?")) {
            showHelp();
          }

          else if (cmd.equals("input")) {
             processCommands(new Scanner(new File(cmdScanner.next().trim())));
          }

          else if (cmd.equals("merge")) {
            int[] odds   = getArray(cmdScanner); 
            int[] evens  = getArray(cmdScanner); 
            int[] result = new int [odds.length + evens.length]; 
            merge(result, odds, evens);
            print1D("merged", result);
          }

          else if (cmd.equals("odd")) {
            int i = cmdScanner.nextInt();
            System.out.println("isOdd(" + i + ") is " + isOdd(i));
          }

          else if (cmd.equals("split")) {
            int[] deck  = getArray(cmdScanner);
            int   evenLen = deck.length / 2;
            int[] evens   = new int[evenLen];
            int   oddLen  = deck.length - evenLen;
            int[] odds    = new int[oddLen];
            split(deck, evens, odds);
            print1D("evens", evens);
            print1D("odds", odds);
          }

          else if (cmd.equals("subsets")) {
            int i = cmdScanner.nextInt();
            trace("Subset size: " + i);
            printSubsets(i);
          }

          else if (cmd.equals("triples")) {
            int i = cmdScanner.nextInt();
            trace("OrderedTriples: " + i);
            printOrderedTriples(i);
          }
          
          else {
            System.err.println("unknown command: " + line);
          }
        }

        catch (NoSuchElementException nsee) {
          System.err.println("syntax error in: " + line);
          cmdScanner.close();
        }

        catch (Exception ex) {
          ex.printStackTrace();
        }
 
        cmdScanner.close();
      }

      lineScanner.close();
    }

   public static void main(String[] args) {
     MyAssign1 ma1 = new MyAssign1();
     System.out.println("Enter commands");
     daysInMonths = ma1.getDaysInMonths();
     cumulative = new int[daysInMonths.length];
     ma1.makeCumulatives(cumulative, daysInMonths);
     ma1.processCommands(new Scanner(System.in));
   }
}
