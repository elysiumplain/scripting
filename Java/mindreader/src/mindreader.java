import java.io.*;
import java.util.*;

public class mindreader {

	public static void main(String[] args) throws IOException
	{
//		Tries is number of times a guess is given
//		choice is the number of choices from each format...
//		... starting with length then first char then final char
		int tries = 1;
		int choice1=0;
		int choice2=0;
		int choice3=0;



		Scanner in;
		in = new Scanner ( System.in );
		File file = null;
		file = new File("words.txt");
		Scanner inFile = new Scanner(file);
		
//		given the array values
		String dict[] = new String[95877];
		int dist[] = new int [95877];


		/*
		 * change from file to array
		 */

		for(int i=0; i < 95877;i++)
		{	
			String currentWord = inFile.nextLine();
			dict[i] = currentWord;
			int a = currentWord.length();
			dist[i] = a;
		}




		System.out.println("Welcome to the Master Mind.");
		System.out.println(" I can read your mind!");
		System.out.println("Think of a word. Any non-plural word from the dictionary, and I will guess your word within 10 questions.");
		System.out.println("How many characters are in your word?");
		int wlength = in.nextInt( );


//		check the length of each word against the length given by the user (wlength= user, dist[k] = word length)
		for(int k=0; k < 95877;k++)
		{
			if(wlength == dist[k])
			{
				choice1++;
			}
		}

		System.out.print("Hmm... That narrows it down to " + choice1 + " choices.");
		System.out.print("What letter does it begin with?");
		String	letb = in.next();
//		Check the first character and the length
		for(int j=0; j < 95877;j++)
		{
			if(letb.charAt(0) == dict[j].charAt(0) && wlength == dist[j])
			{

				choice2++;

			}
		}

		System.out.println("Hmm... That narrows it downto "+choice2 + " choices.");
		System.out.println("What letter does it end with?");
		String lete = in.next();
//		Check the last letter, the first letter and the length
		for(int h=0; h < 95877; h++)
		{
			if(lete.charAt(0) == dict[h].charAt(dict[h].length()- 1) && letb.charAt(0) == dict[h].charAt(0) && wlength == dist[h])
			{
				choice3++;	
			}
		}

		System.out.println("Hmm... That narrows it down to " + choice3 + "choices");

		/*
		 * Loop to print the words and verify if they are correct,
		 * Count to make sure it can only guess 10 words
		 * Break if 10 words or if the correct answer was given.
		 * 
		 */

		for(int h=0; h < 95877; h++)
		{
			if(lete.charAt(0) == dict[h].charAt(dict[h].length()- 1) && letb.charAt(0) == dict[h].charAt(0) && wlength == dist[h])
			{
				String gusto =  dict[h];
				System.out.println("Is your word: " + gusto + " ? Answer 'yes' or 'no'");
				String mmo = in.next();

				if(tries != 10)
				{
					if(mmo.equals("yes"))
					{
						System.out.println("HA! Told you I was smart!");
						break;
					}
					else
					{

						tries++;
					}		
				}
				else
				{
					System.out.println("You outwitted me!");
					break;
				}

			}
		}
	}
}









