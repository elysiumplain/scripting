
/* 
 * Name: Connor Neuhaus
 * e-name: neuhaus
 * date: 10/19/2009
 * Course: CS160
 */
    	 
    	 public class rotate{
    		 public static void main(String[] args){
    			 if(args.length == 0){
    				 System.out.println("......");
    			 System.exit(0);
    		 }
    		int rotation = Integer.parseInt(args[0]);
    		String[] st = new String[args.length-1];
    		rotate cypher = new rotate();
    		for(int i=0;i<st.length;i++){ // this for counts the number of strings in arguments
    			st[i] = "";
    			for (int j=0; j<args[i+1].length();j++) // this for counts the position of the chars in each argument string
    				st[i] = st[i]+cypher.rotchar(rotation, args[i+1].charAt(j));
    		}
    		for(int i=0;i<st.length;i++)
    			System.out.println(st[i]+"");
    	  }//main method
       private char rotchar(int ro, char ch){
        if(64<ch && ch<91)
        	return (char)((ch-65+ro)%26+65);
        else if(96<ch && ch<123)
        	return (char)((ch-97+ro)%26+97);
        else return ch;
       }//Pub class rotate
    }
//pub class Caesar