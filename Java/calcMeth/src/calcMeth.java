/* 
 * Name: Connor Neuhaus
 * e-name: neuhaus
 * date: 10/19/2009
 * Course: CS160
 */

public class calcMeth { 
     public static void main(String[] args){
    	 
    	 final double pi = 3.1415;
    	 final double avo = 6.02214179E23;
    	 calcMeth sc = new calcMeth();
    	 
    	 //check number arguments
          if (args.length == 1){
        	  if (args[0].equalsIgnoreCase("pi"))
        			  System.out.println(pi);
        	  else if (args[0].equalsIgnoreCase("avo"))
        		  System.out.println(avo);
        	  else System.out.println(sc.get_number(args[0]));
          }
          if (args.length == 3){
        	  double[] opd = new double[2];
        	  
        	  for (int i=0,j=0;i<opd.length;i++,j=j+2){
        	  if (args[j].equalsIgnoreCase("pi"))
        			  opd[i]=pi;
        	  else if (args[j].equalsIgnoreCase("avo"))
        		      opd[i]=avo;
        	  else opd[i]=sc.get_number(args[j]);
        	  }
        
        //check operator	  
          if (args[1].equalsIgnoreCase("+"))
        		  System.out.println(sc.operater_plus(opd[0],opd[1]));
        	  else if (args[1].equalsIgnoreCase("-"))
        		  System.out.println(sc.operater_minus(opd[0],opd[1]));
        	  else if (args[1].equalsIgnoreCase("/"))
        		  System.out.println(sc.operater_divide(opd[0],opd[1]));
        	  else if (args[1].equalsIgnoreCase("*"))
        		  System.out.println(sc.operater_multiply(opd[0],opd[1]));
        	  else if (args[1].equalsIgnoreCase(""))
        		  System.out.println(sc.get_number(args[0]));
          
          }
          else System.out.println("Bad Arguments");
          System.out.println(args.length + " arguments");
     }
     
     //methods to do work

     private double operater_plus(double op1,double op2){
    	 return(op1+op2);
     }

     private double operater_minus(double op1,double op2){
    	 return(op1-op2);
     }
     private double operater_divide(double op1,double op2){
    	 return(op1/op2);
     }
     private double operater_multiply(double op1,double op2){
    	 return(op1*op2);
     }
     private double get_number(String st){
    	 return Double.parseDouble(st);
     }
     public boolean isOdd(int n) {
  	   if(n % 2 == 0)
  		   return false;
  	   else
       return true;
     }
     
}


/*

public class JavaMethod {
    public static void main (String [] args) {
        JavaMethod JavaMethod = new JavaMethod();
    int[] a = JavaMethod.createArray();
    JavaMethod.printArray(a);
        System.out.println("\n == Summary == ");
        System.out.println("Sum: "+JavaMethod.getSum(a));
        System.out.println("Max: "+JavaMethod.getMax(a));
        System.out.println("Average: "+JavaMethod.getAverage(a));
    }
    
    public int[] createArray() {
        int[] x = {1,2,3,4,5,6};
        return x;
    }

    public void printArray(int[] a) {
        for (int i=0;i<a.length;i++)
            System.out.println(i+"th: "+a[i]);
        
    }
    
    public int getSum(int[] a) {
        int sum = 0;
        for (int i=0;i<a.length;i++){
            sum = sum + a[i];
        }
        return sum;
    }
    
    public double getAverage(int[] a) {
        return (double) getSum(a)/a.length;
    }
    
    public int getMax(int[] a) {
        int max = 0;
        for (int i=0;i<a.length;i++)
            if(max<a[i])
                max=a[i];
            return max;
        
    }
}


*/