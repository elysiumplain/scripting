import requests
import time
import click
from re import Pattern, search
from bs4 import BeautifulSoup
from enum import Enum


class RequestDelay(Enum):
    SLOW = 10
    MEDIUM = 2
    FAST = 0


@click.command()
@click.option('--url', help="URL to run scan against")
@click.option('--speed', type=click.Choice(list(map(lambda i: i.value, RequestDelay))), default=RequestDelay.MEDIUM.value, help="Time between requests")
@click.option("--useragent", default="CustomAgent", help="Set custom user-agent")
@click.option('--verbose', default=True, help="Set verbosity")
@click.option('--email', 'email', flag_value='email', help="Show emails gathered from spider")
@click.option("--pattern", default=Pattern("^[a-zA-Z]{4,}$"), help="Set custom RegEx for word filtering")
@click.option("--depth", default=click.Choice(range(1, 3)), help="How many layers deep to transit from the base URL")
def cli(url: str, depth, speed, verbose: bool, pattern: Pattern, useragent: str, email: bool) -> None:
    """Scrape a URL using a specific user-agent to compile a list of keywords.

    :param depth: how many layers deep to transit from the base URL
    :param pattern: set custom RegEx for word filtering
    :param url: url to scrape
    :param speed: delay time between page parsings
    :param verbose: output details of the process
    :param useragent: specify the browser (agent) you wish to use when executing the request
    :param email: collect valid email addresses from the scrape

    :return: None
    """

    linklist = []
    wordlist = []
    emaillist = []
    authorlist = []

    linklist.append(url)
    if verbose:
        print(linklist)
    linklist = GetLinks(url, linklist, verbose, useragent)
    for site in linklist:

        time.sleep(speed)

        wordlist, emaillist, authorlist = ParsePage(site, pattern, wordlist, emaillist, authorlist, verbose, useragent)
    # TODO: Create a function to output to file
    print(wordlist)
    if email:
        print(emaillist)


def AddLinkToLinkList(url, linklist, verbose):
    if url in linklist:
        if verbose:
            print(f"URL: {url} already in list")
    else:
        urlRoot = url.split("/")
        domainRoot = linklist[0].split("/")
        if urlRoot[2] == domainRoot[2]:
            linklist.append(url)
            if verbose:
                print(f"URL: {url} added to list")
        else:
            if verbose:
                print(f"URL: {url} is not in scope")
    return linklist


def AddToWordList(wordPattern, word, wordlist, emaillist, authorlist, verbose, emailPattern: Pattern = Pattern(".+\@.+\..+")):
    words = word.split(" ")
    for w in words:
        if w not in wordlist:
            if search(emailPattern, w):
                emaillist.append(w)
            if search(wordPattern, w):
                wordlist.append(w)
            else:
                if verbose:
                    print(f"Word: {w} is not a word")
    return wordlist, emaillist, authorlist


def ParsePage(url, pattern, wordlist, emaillist, authorlist, verbose, useragent):
    print(f"Page {url} is being scanned")
    try:
        headers = {'User-Agent': useragent}
        req = requests.get(url, headers)
        soup = BeautifulSoup(req.text, 'html.parser')
        for text in soup.stripped_strings:
            wordlist, emaillist, authorlist = AddToWordList(pattern, text, wordlist, emaillist, authorlist, verbose)
        return wordlist, emaillist, authorlist
    except Exception as ex:
        if verbose:
            print(ex)


def GetLinks(url, linklist, verbose, useragent):
    headers = {'User-Agent': useragent}
    req = requests.get(url, headers)
    soup = BeautifulSoup(req.text, 'html.parser')
    anchors = soup.find_all('a')

    for a in anchors:
        try:
            newURL = f"{a.attrs['href']}"
            r = search("\.", newURL)
            if r:
                pass
            else:
                # TODO: Need to parse out the root of the base url so it can append better here if the url entered is not a root url.
                newURL = f"{url}{a.attrs['href']}"
            linklist = AddLinkToLinkList(newURL, linklist, verbose)
        except Exception as ex:
            if verbose:
                print(f"Couldn't add {newURL} to linklist")
    return linklist


if __name__ == "__main__":
    cli()
