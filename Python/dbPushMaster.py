#####################################
# 
# 
# 
# 
# 
# 
# 
#####################################
# -this will handle
#     -access to the DB
#    -queries 
#     -pushing data
#         -unwraps list into insert to given tables
#####################################


import mysql.connector
from datetime import datetime
from mysql.connector import errorcode


class dbPush:
    def __init__(self, host, logFile, queLog):
        self.host      = host
        self.logFile   = logFile
        self.queLog    = queLog
        self.config    = {
            'user'     : 'fsi_01',
            'password' : 'ToolDataStream',
            'host'     :  self.host,
   #         'database' : 'fsi_data',
        }


    def log(self, msg):
        #print( msg )
        file = open(self.logFile, 'a')
        file.write('\nDBPUSH: {}'.format(msg) )
        file.close()


    def update(self, instruction):
        try:
            self.connect()
            self.cursor.execute(instruction)
            self.cnx.commit()
            self.close()
        except Exception as e:
            print(e)
        return

    def query(self, instruction):
        try:
            self.connect()
            self.cursor.execute(instruction)
            temp = list(self.cursor)
            self.close()
            return( temp )
        except Exception as e: 
            self.queLog.put([ datetime.now(), 'ERROR: dbPush.query', '{} : {}'.format(instruction, e) ])


    def connect(self):
        try:
            self.cnx = mysql.connector.connect ( **self.config )
            self.cursor = self.cnx.cursor()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                self.log('incorrect username or pw')
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                self.log('Database does not exist')
            else:
                self.log('connect: {}'.format(err))
        else:
            pass
            #print('Successful connection')

    def close(self):
        self.cursor.close()
        self.cnx.close()

    def send(self, statement, data):
        try:
##$
 #           print('Stmt: {}'.format(statement))
 #           print('data: {}'.format(data))
            self.connect()
            self.cursor.execute( statement.format( ','.join(str(i) for i in data) ) )
            #self.cursor.execute( self.push.format('2018-12-19',51) )
            self.cnx.commit()
            self.close()
        except Exception as e: 
            self.queLog.put([ datetime.now(), 'ERROR: dbPush.send', '{} : {}'.format(statement, e) ])

    def sendData(self, table, data):
        try:
            self.connect()
            self.cursor.execute( "INSERT INTO `fsi_data`.`{}` (`Timest`, `value`) VALUES {}".format( table, ','.join(str(i) for i in data) ) )
            #self.cursor.execute( self.push.format('2018-12-19',51) )
            self.cnx.commit()
            self.close()
        except Exception as e: 
            self.queLog.put([ datetime.now(), 'ERROR: dbPush.sendData', '{} : {}'.format(table, e) ])

    def sendSimple(self, table, station, data):
        try:
            self.connect()
            self.cursor.execute( "INSERT INTO `fsi_data`.`{}` (`sensor_id`, `ts`, `value(ul/s)`) VALUES {}".format( table, ','.join(str( (station,) + i) for i in data) ) )
            self.cnx.commit()
            self.close()
        except Exception as e:
            self.queLog.put([ datetime.now(), 'ERROR: dbPush.sendSimple', '{} : {}'.format(station, e) ])
    

    def sendSummary(self, table, station, data):
        try:
            self.connect()
            self.cursor.execute( "INSERT INTO `fsi_data`.`{}` (`sensor_id`, `start`, `end`, `avg(ul/s)`, `delta(sec)`, `area(ul)`) VALUES {}".format(table, ','.join(str( (station,) + i) for i in data) ) )
            self.cnx.commit()
            self.close()
        except Exception as e:
            self.queLog.put([ datetime.now(), 'ERROR: dbPush.sendSummary', '{} : {}'.format(station, e) ])

    











