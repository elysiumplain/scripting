import bs4
import requests
import os
import webbrowser
import pprint
import random

class ScpllScraper:
    
    def __init__(self, standard=['detail','MARCdetail','ISBDdetail'], mywebsite='https://catalog.saclaw.org/cgi-bin/koha/opac-{}.pl?biblionumber={}'):
        #NORMAL ---- https://catalog.saclaw.org/cgi-bin/koha/opac-detail.pl?biblionumber=9406
        #MARC ------ https://catalog.saclaw.org/cgi-bin/koha/opac-MARCdetail.pl?biblionumber=9406
        #ISBN ------ https://catalog.saclaw.org/cgi-bin/koha/opac-ISBDdetail.pl?biblionumber=9406
        self.standard = standard
        self.mywebsite = mywebsite
        self.pages = {}

    def sendRequest(self, mywebsite, standard, page):
        
        if page in self.pages.keys():
            return
        
        for std in standard:
            mywebsite = mywebsite.format(std, page)
            #print(mywebsite, std, self.standard[0])
            
            res = requests.get(mywebsite)
            
            if res.status_code == requests.codes.ok:
                
                #print('retrieved content of length: ', len(res.text), '\r\n')
                #print('looking like: ', res.text[:250], '\r\n')
                html = bs4.BeautifulSoup(res.text, features="html.parser")
                
                main = html.find('div', attrs={'class':'maincontent'})
                
                main_record = None
                if std == self.standard[0]:
                    main_record = main.find('div', attrs={'class':'record'})
                    title = main_record.find('h2', attrs={'class':'title'})
                    #print(main_record)
                elif std == self.standard[1]:
                    main_record = main.find('div', attrs={'id':'labeledmarc'})
                    title = main.find('h1', attrs={'class':'title'})
                    #print(main_record)
                elif std == self.standard[2]:
                    main_record = main.find('div', attrs={'id':'isbdcontents'})
                    #title = main_record.find('h2', attrs={'class':'title'})
                    #print(main_record)
                else:
                    self.pages[page] = 'NO MATCHING STANDARD!!!'
                    return
                
                # build your object structure however you like so retrieval is easier!
                self.pages[page] = {'title': title, std: {'soup': main_record}}
            
            else:
                print('FAILED TO RETRIEVE: ', mywebsite, '\r\n')
                print('WEBSITE RESPONSE: ', res.status_code, '\r\n')
                if page in self.pages.keys():
                    self.pages[page][std] = res.status_code
                else:
                    self.pages[page] = {std: res.status_code}

####this is where execution begins...
myscraper = ScpllScraper()

#this dumps webpage content into a holder called "pages"
for i in range(0,10):#loop some number of times
    pageNum = random.randint(0,9999)#generate a random page number
    myscraper.sendRequest(myscraper.mywebsite, [myscraper.standard[0]], pageNum)#request resource webpage and parse out the content format you care about

#pprint.pprint(myscraper.pages)#print the pages we retrieved from myscraper Class
print(myscraper.pages.keys())#just show me which page numbers I tried
#[print(p.get('title')) for p in myscraper.pages.values()]#just show me which titles I got
[print({k:p.get('title')}) for k,p in myscraper.pages.items()]#show me titles and page number I got








