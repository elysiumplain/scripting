The goal of the program is to crawl web content.

Interfacing with a webserver uses either the HTTP protocol or FTP protocol. Both of these protocols require a port.

Therefore, the Abstract Interface class will require specifying which protocol, a port, and a crawler (another abstract interface) to define behavior.

The Crawler will likely need a manager, as each may wish to generate many different crawlers operating asynchronously.

#######
class AbstractSpiderFactory():
    """ Abstract Factory for generating a spider to crawl web content."""
    def c_protocol(self):
        pass
    def c_port(self):
        pass
    def c_crawler(self):
        pass

class HTTPConcreteFactory(AbstractSpiderFactory):
    """ Factory for crawling web content using the HTTP connection protol."""
class FTPConcreteFactory(AbstractSpiderFactory):
    """ Factory for crawling web content using the FTP connection protol.

#######
class ProtocolAbstractProduct():

class HTTPProtocol(ProtocolAbstractProduct):
class FTPProtocol(ProtocolAbstractProduct):

class HTTPSecureProtocol(ProtocolAbstractProduct):
class FTPSecureProtocol(ProtocolAbstractProduct):

#######
class PortAbstractProduct():

class HTTPPort(PortAbstractProduct):
class FTPPort(PortAbstractProduct):

#######
class CrawlerAbstractProduct():

class HTTPCrawler(CrawlerAbstractProduct):
class FTPCrawler(CrawlerAbstractProduct):



The execution point for any of these classes will be a Connector class (possibly as a connection manager)

#######
class Connector():
    def __init__(self, abstractspiderfactory):
        self.protocol=abstractspiderfactory.c_protocol()
        self.port=abstractspiderfactory.c_port()
        self.crawler=abstractspiderfactory.c_crawler()



AbstractSpiderFactory will require all input parameters necessary to decide between classes which exist on the same logic layer, horizontally. 
For example, in the current design the Protocol class may either be "secure" (HTTPS) or default (HTTP).

Let's refactor our abstract factory interface:
#######
class AbstractSpiderFactory():
    """ Abstract Factory for generating a spider to crawl web content."""
    def __init__(self, is_secure):
        self.is_secure = is_secure
    def c_protocol(self):
        pass
    def c_port(self):
        pass
    def c_crawler(self):
        pass


Now put it all together into an entrypoint and we get a factory pattern.

#######
def main(myIni):
    myIni = readSomeIni(myIni)
    is_secure = myIni.getSecurityLevel()
    protocol = myIni.getProtocol()
    if protocol == "http"
        if is_https:
            myASF = HTTPConcreteFactory(is_secure)  # AbstractSpiderFactory
    else
        pass
    myConnector = Connector(myASF)
    resp = myConnector.connect()
    res = myConnector.crawl(resp)
    
    pprint.pprint(res)
