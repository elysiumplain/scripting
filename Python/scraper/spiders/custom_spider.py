import scrapy
from optparse import OptionParser, OptionGroup

class CustomSpider(scrapy.Spider):
    
    def __init__(self):
        self.name = 'CustomBot'
        self.allowed_domains = ['www.reddit.com/r/gameofthrones/']
        self.start = ['http://www.reddit.com/r/gameofthrones//']
        #specify the lable & extractor of content to extract using css selectors
        self.targets = {
            'title':'.title.may-blank::text',
            'vote':'.score.unvoted::text',
            'created_at':'time::attr(title)',
            'comments':'.comments::text',
            #'image':'img::attr(data-img)',
            }
    
    def getAsCols(self):
        scraped_info = self.targets
        for k,v in self.targets.items():
            scraped_info[k] = response.css(v).extract()
        return scraped_info
    
    def getAsRows(self, res=None, ondemand=False):
        if not res:
            res = self.getAsCols()
        #Give the extracted content row wise
        if ondemand:
            for item in zip(*res):
                #use existing targets dictionary to store the scraped info
                scraped_info = self.targets
                scraped_info.update({
                    'title' : item[0],
                    'vote' : item[1],
                    'created_at' : item[2],
                    'comments' : item[3],}
                )

                #yield or give the scraped info to scrapy
                yield scraped_info
        else:
            return zip(*res)

def main():
    scraper = CustomSpider()
    res = scraper.getAsCols()
    print(res)

def get_options():
    parser = OptionParser()
    
    parser.add_option("-o","--output",help="Output file",dest="output")
    parser.add_option("-i","--input",help="Input destination",dest="output")
    parser.add_option("-o","--output",help="Output file",dest="output")

if __name__ == "__main__":
	main()