
import smtplib
from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import csv
from tabulate import tabulate
from email.mime.image import MIMEImage
import os
import sys
import pandas as pd
import urllib
from PIL import Image
from io import StringIO
import datetime
#import psycopg2
from email.mime.application import MIMEApplication
import pickle
import glob



def send_mail(use_case_email, use_case_subject, emails, folderpath = '', df = None):#, emails, param_value, standardize, cols, outlier_method, targ, ts, err,subj):
    s = smtplib.SMTP()
    s.set_debuglevel(1)
    #s.ehlo()
    s.connect("myrelay.domain.com",465)
    #s.starttls()
    s.ehlo()
    #listofimages = glob.glob(folderpath+"/*.jpg")  #+glob.glob(folderpath+"/*.csv")
    dfbody = df
    #pd.read_csv(folderpath+"/anolist.csv")
    #imgs = dfbody["ImageName"].tolist()
    #listofimages = [ folderpath+"/"+i for i in imgs ]
    #print(emails)
    for i in [[1]]:


        buffer = StringIO()
        buffer.seek(0)

        html2 = """<HTML><head><style> table {{font-family: arial, sans-serif; border-collapse: collapse; width: 100%; }}td  {{     border: 1px solid #d2dfdd;   color: black;  text-align: center;     padding: 8px; }} th {{     background-color: silver; border: 1px solid black;     text-align: center;     padding: 8px; }} tr:nth-child(even) {{     background-color: #12f235; }} </style> </head> <body>     <h1>Photoflow Errors</h1>     <table>         {xxx}     </table> </body> </HTML>"""
        tr = "<tr>{0}</tr>"
        td = "<td>{0}</td>"
        th = "<th>{0}</th>"

        if isinstance(dfbody, pd.DataFrame):
            data=[ dfbody.columns.tolist()]
            data.extend(dfbody.values.tolist()) #,['Path',path],['Filename',ipfile]]
    #        print(data)
            firstitem = [tr.format(''.join([th.format(a) for a in item])) for item in data[:1]]
            subitems = [tr.format(''.join([td.format(a) for a in item])) for item in data[1:]]
            firstitem.extend(subitems)
            html2=html2.format(xxx="".join(firstitem))
        else:
            html2=dfbody


        #text = text.format(table=tabulate(data, headers="firstrow", tablefmt="grid"))
        #html = html.format(table=tabulate(data, headers="firstrow", tablefmt="html"))
#
        message=MIMEMultipart()
        body=MIMEMultipart('mixed')
        body.attach(MIMEText(html2,'html'))
        message.attach(body)


        #for image in listofimages:
            #cover_letter = MIMEApplication(open(image, "rb").read())
            #cover_letter.add_header('Content-Disposition', 'attachment', filename=image.rsplit('/', 1)[1])
            #message.attach(cover_letter)


        reci = emails.split(",")

        message['From']=use_case_email
        message['To']=", ".join(reci)
        message['Subject']=use_case_subject
        s.sendmail("sender@domain.com", reci, message.as_string())

    s.quit()

#if __name__==__main__:
#    sendAlertEmail.send_mail("Notifications@wdc.com", 'Your Specific Notifications', users,'', df_email)

